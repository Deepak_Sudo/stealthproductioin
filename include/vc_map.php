<?php

if ( function_exists( 'vc_map' ) ) {
	
vc_map( array(
	'name' => 'Title',
	'base' => 'title',
	'icon' => 'icon-vc-clapat-grenada',
	'is_container' => 'true',
	'category' => esc_html__('Grenada - Typo Elements', 'grenada'),
	'description' => esc_html__('Title', 'grenada'),
	'admin_enqueue_css' => array( get_template_directory_uri() . '/include/vc-extend.css' ),
	'params' => array(
		array(
			'type' => 'dropdown',
			'holder' => 'div',
			'heading' => esc_html__('Title Size', 'grenada'),
			'description' => '',
			'param_name' => 'size',
			'value' => array( 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'),
		),
		array(
			'type' => 'dropdown',
			'holder' => 'div',
			'heading' => esc_html__('Has Underline?', 'grenada'),
			'description' => esc_html__('If the title is displayed underlined or without underline', 'grenada'),
			'param_name' => 'underline',
			'value' => array( 'no', 'yes'),
		),
		array(
			'type' => 'dropdown',
			'holder' => 'div',
			'heading' => esc_html__('Big Title?', 'grenada'),
			'description' => esc_html__('This parameter applies only for H1 headings. If the title is normal or bigger title font size.', 'grenada'),
			'param_name' => 'big',
			'value' => array( 'no', 'yes'),
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Extra Class Name', 'grenada'),
			'description' => esc_html__('Add here any extra CSS class name(s). Separate multiple classes with space.', 'grenada'),
			'param_name' => 'extra_class_name',
			'value' => '',
		),
		array(
			'type' => 'textarea_html',
			'holder' => 'div',
			'heading' => esc_html__('Content', 'grenada'),
			'param_name' => 'content',
			'value' => esc_html__('Content goes here', 'grenada'),
		),
	)
) );

vc_map( array(
	'name' => 'Button',
	'base' => 'button',
	'icon' => 'icon-vc-clapat-grenada',
	'is_container' => 'true',
	'category' => esc_html__('Grenada - Typo Elements', 'grenada'),
	'description' => esc_html__('Button', 'grenada'),
	'params' => array(
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Button Link', 'grenada'),
			'description' => esc_html__('URL for the button', 'grenada'),
			'param_name' => 'link',
			'value' => 'http://',
		),
		array(
			'type' => 'dropdown',
			'holder' => 'div',
			'heading' => esc_html__('Target Window', 'grenada'),
			'description' => esc_html__('Button link opens in a new or current window', 'grenada'),
			'param_name' => 'target',
			'value' => array( '_blank', '_self'),
		),
		array(
			'type' => 'dropdown',
			'holder' => 'div',
			'heading' => esc_html__('Button type', 'grenada'),
			'description' => '',
			'param_name' => 'type',
			'value' => array( 'normal', 'outline'),
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Extra Class Name', 'grenada'),
			'description' => esc_html__('Add here any extra CSS class name(s). Separate multiple classes with space.', 'grenada'),
			'param_name' => 'extra_class_name',
			'value' => '',
		),
		array(
			'type' => 'textarea_html',
			'holder' => 'div',
			'heading' => esc_html__('Content', 'grenada'),
			'param_name' => 'content',
			'value' => esc_html__('Content goes here', 'grenada'),
		),
		)
) );

vc_map( array(
	'name' => 'Space Between Buttons',
	'base' => 'space_between_buttons',
	'icon' => 'icon-vc-clapat-grenada',
	'category' => esc_html__('Grenada - Typo Elements', 'grenada'),
	'description' => esc_html__('Adds a space between two button shortcodes', 'grenada'),
	'show_settings_on_create' => false
) );

vc_map( array(
	'name' => 'Icon Service',
	'base' => 'service',
	'icon' => 'icon-vc-clapat-grenada',
	'is_container' => 'true',
	'category' => esc_html__('Grenada - Elements', 'grenada'),
	'description' => esc_html__('Service Box', 'grenada'),
	'params' => array(
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Icon', 'grenada'),
			'description' => esc_html__('Icon displayed within service box. Type in the class of the icon in this edit box. The complete and latest list: http://fortawesome.github.io/Font-Awesome/icons/', 'grenada'),
			'param_name' => 'icon',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Service Title', 'grenada'),
			'description' => esc_html__('Title of the service', 'grenada'),
			'param_name' => 'title',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Extra Class Name', 'grenada'),
			'description' => esc_html__('Add here any extra CSS class name(s). Separate multiple classes with space.', 'grenada'),
			'param_name' => 'extra_class_name',
			'value' => '',
		),
		array(
			'type' => 'textarea_html',
			'holder' => 'div',
			'heading' => esc_html__('Content', 'grenada'),
			'param_name' => 'content',
			'value' => esc_html__('Content goes here', 'grenada'),
		),
	)
) );

vc_map( array(
	'name' => 'Contact Info Box',
	'base' => 'contact_box',
	'icon' => 'icon-vc-clapat-grenada',
	'is_container' => 'true',
	'category' => esc_html__('Grenada - Elements', 'grenada'),
	'description' => esc_html__('Contact Info  Box', 'grenada'),
	'params' => array(
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Icon', 'grenada'),
			'description' => esc_html__('Icon displayed within contact info box. Type in the class of the icon in this edit box. The complete and latest list: http://fortawesome.github.io/Font-Awesome/icons/', 'grenada'),
			'param_name' => 'icon',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Title', 'grenada'),
			'description' => esc_html__('Title or header of the contact info box', 'grenada'),
			'param_name' => 'title',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Extra Class Name', 'grenada'),
			'description' => esc_html__('Add here any extra CSS class name(s). Separate multiple classes with space.', 'grenada'),
			'param_name' => 'extra_class_name',
			'value' => '',
		),
		array(
			'type' => 'textarea_html',
			'holder' => 'div',
			'heading' => esc_html__('Contact Info', 'grenada'),
			'param_name' => 'content',
			'value' => esc_html__('Contact info goes here', 'grenada'),
		),
	)
) );

vc_map( array(
	'name' => 'Map',
	'base' => 'grenada_map',
	'icon' => 'icon-vc-clapat-grenada',
	'category' => esc_html__('Grenada - Elements', 'grenada'),
	'description' => esc_html__('Adds a google map with settings from theme options.', 'grenada'),
	'show_settings_on_create' => false
) );

vc_map( array(
	'name' => 'Normal Image Slider',
	'base' => 'general_slider',
	'icon' => 'icon-vc-clapat-grenada',
	'as_parent' => array('only' => 'general_slide'),'category' => esc_html__('Grenada - Sliders', 'grenada'),
	'description' => esc_html__('Normal Image Slider', 'grenada'),
	'content_element' => true,
	'show_settings_on_create' =>true,
	"params" => array(
        array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Extra Class Name', 'grenada'),
			'description' => esc_html__('Add here any extra CSS class name(s). Separate multiple classes with space.', 'grenada'),
			'param_name' => 'extra_class_name',
			'value' => '',
		),
    ),
	'js_view' => 'VcColumnView'
) );

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {class WPBakeryShortCode_general_slider extends WPBakeryShortCodesContainer {}}

vc_map( array(
	'name' => 'Slide',
	'base' => 'general_slide',
	'icon' => 'icon-vc-clapat-grenada',
	'as_child' => array('only' => 'general_slider' ),'category' => esc_html__('Grenada - Sliders', 'grenada'),
	'description' => esc_html__('Slide', 'grenada'),
	'params' => array(
		array(
			'type' => 'attach_image',
			'holder' => 'div',
			'heading' => esc_html__('Slider Image', 'grenada'),
			'description' => esc_html__('Image representing this slide', 'grenada'),
			'param_name' => 'img_id',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Image ALT', 'grenada'),
			'description' => esc_html__('The ALT attribute specifies an alternate text for an image, if the image cannot be displayed', 'grenada'),
			'param_name' => 'alt',
			'value' => '',
		),
	)
) );

if ( class_exists( 'WPBakeryShortCode' ) ) {class WPBakeryShortCode_general_slide extends WPBakeryShortCode {}}


vc_map( array(
	'name' => 'Carousel Image Slider',
	'base' => 'carousel_slider',
	'icon' => 'icon-vc-clapat-grenada',
	'as_parent' => array('only' => 'carousel_slide'),'category' => esc_html__('Grenada - Sliders', 'grenada'),
	'description' => esc_html__('Carousel Image Slider', 'grenada'),
	'content_element' => true,
	'show_settings_on_create' =>true,
	"params" => array(
        array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Extra Class Name', 'grenada'),
			'description' => esc_html__('Add here any extra CSS class name(s). Separate multiple classes with space.', 'grenada'),
			'param_name' => 'extra_class_name',
			'value' => '',
		),
    ),
	'js_view' => 'VcColumnView'
) );

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {class WPBakeryShortCode_carousel_slider extends WPBakeryShortCodesContainer {}}

vc_map( array(
	'name' => 'Carousel Slide',
	'base' => 'carousel_slide',
	'icon' => 'icon-vc-clapat-grenada',
	'as_child' => array('only' => 'carousel_slider' ),
	'category' => esc_html__('Grenada - Sliders', 'grenada'),
	'description' => esc_html__('Carousel Slide', 'grenada'),
	'params' => array(
		array(
			'type' => 'attach_image',
			'holder' => 'div',
			'heading' => esc_html__('Slider Image', 'grenada'),
			'description' => esc_html__('Image representing this slide', 'grenada'),
			'param_name' => 'img_id',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Image ALT', 'grenada'),
			'description' => esc_html__('The ALT attribute specifies an alternate text for an image, if the image cannot be displayed', 'grenada'),
			'param_name' => 'alt',
			'value' => '',
		),
	)
) );

if ( class_exists( 'WPBakeryShortCode' ) ) {class WPBakeryShortCode_carousel_slide extends WPBakeryShortCode {}}


vc_map( array(
	'name' => 'Image Collage',
	'base' => 'clapat_collage',
	'icon' => 'icon-vc-clapat-grenada',
	'as_parent' => array('only' => 'clapat_collage_image'),
	'category' => esc_html__('Grenada - Elements', 'grenada'),
	'description' => esc_html__('Collage with image popup', 'grenada'),
	'content_element' => true,
	'show_settings_on_create' => true,
	"params" => array(
        array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Extra Class Name', 'grenada'),
			'description' => esc_html__('Add here any extra CSS class name(s). Separate multiple classes with space.', 'grenada'),
			'param_name' => 'extra_class_name',
			'value' => '',
		),
    ),
	'js_view' => 'VcColumnView'
) );

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {class WPBakeryShortCode_clapat_collage extends WPBakeryShortCodesContainer {}}

vc_map( array(
	'name' => 'Collage Image',
	'base' => 'clapat_collage_image',
	'icon' => 'icon-vc-clapat-grenada',
	'as_child' => array('only' => 'clapat_collage' ),
	'category' => esc_html__('Grenada - Elements', 'grenada'),
	'description' => esc_html__('Collage Image', 'grenada'),
	'params' => array(
		array(
			'type' => 'attach_image',
			'holder' => 'div',
			'heading' => esc_html__('Collage Image Thumbnail', 'grenada'),
			'description' => esc_html__('Thumbnail image representing this ollage item, included in carousel and linking a high res version.', 'grenada'),
			'param_name' => 'thumb_img_id',
			'value' => '',
		),
		array(
			'type' => 'attach_image',
			'holder' => 'div',
			'heading' => esc_html__('Collage Image', 'grenada'),
			'description' => esc_html__('Image representing this collage item opening in a popup', 'grenada'),
			'param_name' => 'img_id',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Image ALT', 'grenada'),
			'description' => esc_html__('The ALT attribute specifies an alternate text for an image, if the image cannot be displayed', 'grenada'),
			'param_name' => 'alt',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Collage Image Caption', 'grenada'),
			'description' => esc_html__('The caption of this collage item', 'grenada'),
			'param_name' => 'info',
			'value' => '',
		),
	)
) );

if ( class_exists( 'WPBakeryShortCode' ) ) {class WPBakeryShortCode_clapat_collage_image extends WPBakeryShortCode {}}


vc_map( array(
	'name' => 'Popup Image',
	'base' => 'clapat_popup_image',
	'icon' => 'icon-vc-clapat-grenada',
	'category' => esc_html__('Grenada - Elements', 'grenada'),
	'description' => esc_html__('Image Popup', 'grenada'),
	'params' => array(
		array(
			'type' => 'attach_image',
			'holder' => 'div',
			'heading' => esc_html__('Zoomed Image', 'grenada'),
			'description' => esc_html__('Zoomed image - usually a higher res image', 'grenada'),
			'param_name' => 'img_id',
			'value' => '',
		),
		array(
			'type' => 'attach_image',
			'holder' => 'div',
			'heading' => esc_html__('Thumbnail Image', 'grenada'),
			'description' => esc_html__('The thumbnail', 'grenada'),
			'param_name' => 'thumb_id',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Extra Class Name', 'grenada'),
			'description' => esc_html__('Add here any extra CSS class name(s). Separate multiple classes with space.', 'grenada'),
			'param_name' => 'extra_class_name',
			'value' => '',
		),
	)
) );

if ( class_exists( 'WPBakeryShortCode' ) ) {class WPBakeryShortCode_clapat_popup_image extends WPBakeryShortCode {}}


vc_map( array(
	'name' => 'Captioned Image',
	'base' => 'clapat_captioned_image',
	'icon' => 'icon-vc-clapat-grenada',
	'category' => esc_html__('Grenada - Elements', 'grenada'),
	'description' => esc_html__('Captioned Image', 'grenada'),
	'params' => array(
		array(
			'type' => 'attach_image',
			'holder' => 'div',
			'heading' => esc_html__('Image', 'grenada'),
			'description' => esc_html__('The image', 'grenada'),
			'param_name' => 'img_id',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('ALT', 'grenada'),
			'description' => esc_html__('ALT attribute.', 'grenada'),
			'param_name' => 'alt',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Caption', 'grenada'),
			'description' => esc_html__('Image caption.', 'grenada'),
			'param_name' => 'caption',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Extra Class Name', 'grenada'),
			'description' => esc_html__('Add here any extra CSS class name(s). Separate multiple classes with space.', 'grenada'),
			'param_name' => 'extra_class_name',
			'value' => '',
		),
	)
) );

if ( class_exists( 'WPBakeryShortCode' ) ) {class WPBakeryShortCode_clapat_captioned_image extends WPBakeryShortCode {}}


vc_map( array(
	'name' => 'Testimonials',
	'base' => 'testimonials',
	'icon' => 'icon-vc-clapat-grenada',
	'as_parent' => array('only' => 'testimonial'),
	'category' => esc_html__('Grenada - Elements', 'grenada'),
	'description' => esc_html__('Testimonials Carousel', 'grenada'),
	'content_element' => true,
	'show_settings_on_create' => true,
	"params" => array(
        // add params same as with any other content element
        array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Extra Class Name', 'grenada'),
			'description' => esc_html__('Add here any extra CSS class name(s). Separate multiple classes with space.', 'grenada'),
			'param_name' => 'extra_class_name',
			'value' => '',
		),
    ),
	'js_view' => 'VcColumnView'
) );

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {class WPBakeryShortCode_testimonials extends WPBakeryShortCodesContainer {}}

vc_map( array(
	'name' => 'Testimonial',
	'base' => 'testimonial',
	'icon' => 'icon-vc-clapat-grenada',
	'as_child' => array('only' => 'testimonials' ),
	'category' => esc_html__('Grenada - Elements', 'grenada'),
	'description' => esc_html__('Testimonial', 'grenada'),
	'params' => array(
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Name', 'grenada'),
			'description' => esc_html__('Name of the person or company this testimonial belongs to.', 'grenada'),
			'param_name' => 'name',
			'value' => '',
		),
		array(
			'type' => 'textarea_html',
			'holder' => 'div',
			'heading' => __('Content', 'grenada'),
			'param_name' => 'content',
			'value' => __('Content goes here', 'grenada'),
		),
	)
) );

if ( class_exists( 'WPBakeryShortCode' ) ) {class WPBakeryShortCode_testimonial extends WPBakeryShortCode {}}


vc_map( array(
	'name' => 'Clients',
	'base' => 'clients',
	'icon' => 'icon-vc-clapat-grenada',
	'as_parent' => array('only' => 'client_item'),
	'category' => esc_html__('Grenada - Elements', 'grenada'),
	'description' => esc_html__('Clients List', 'grenada'),
	'content_element' => true,
	'show_settings_on_create' => true,
	"params" => array(
        // add params same as with any other content element
        array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Extra Class Name', 'grenada'),
			'description' => esc_html__('Add here any extra CSS class name(s). Separate multiple classes with space.', 'grenada'),
			'param_name' => 'extra_class_name',
			'value' => '',
		),
    ),
	'js_view' => 'VcColumnView'
) );

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {class WPBakeryShortCode_clients extends WPBakeryShortCodesContainer {}}

vc_map( array(
	'name' => 'Client',
	'base' => 'client_item',
	'icon' => 'icon-vc-clapat-grenada',
	'as_child' => array('only' => 'clients' ),
	'category' => esc_html__('Grenada - Elements', 'grenada'),
	'description' => esc_html__('Client Logo or Image', 'grenada'),
	'params' => array(
		array(
			'type' => 'attach_image',
			'holder' => 'div',
			'heading' => esc_html__('Client Logo or Image', 'grenada'),
			'description' => esc_html__('The client logo', 'grenada'),
			'param_name' => 'img_id',
			'value' => '',
		),
	)
) );

if ( class_exists( 'WPBakeryShortCode' ) ) {class WPBakeryShortCode_client_item extends WPBakeryShortCode {}}

vc_map( array(
	'name' => 'Video Hosted',
	'base' => 'grenada_video',
	'icon' => 'icon-vc-clapat-grenada',
	'category' => esc_html__('Grenada - Elements', 'grenada'),
	'description' => esc_html__('Self hosted video', 'grenada'),
	'params' => array(
		array(
			'type' => 'attach_image',
			'holder' => 'div',
			'heading' => esc_html__('Cover Image', 'grenada'),
			'description' => esc_html__('Cover image for still video', 'grenada'),
			'param_name' => 'cover_img_id',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Webm URL', 'grenada'),
			'description' => esc_html__('URL of the self hosted video in webm format', 'grenada'),
			'param_name' => 'webm_url',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Mp4 URL', 'grenada'),
			'description' => esc_html__('URL of the self hosted video in mp4 format', 'grenada'),
			'param_name' => 'mp4_url',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Extra Class Name', 'grenada'),
			'description' => esc_html__('Add here any extra CSS class name(s). Separate multiple classes with space.', 'grenada'),
			'param_name' => 'extra_class_name',
			'value' => '',
		),
	)
) );

if ( class_exists( 'WPBakeryShortCode' ) ) {class WPBakeryShortCode_grenada_video extends WPBakeryShortCode {}}

} // if vc_map function exists
?>