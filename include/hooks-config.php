<?php
/**
 * Created by Clapat
 * Date: 29/07/16
 * Time: 11:00 AM
 */

 // Extra classes to the body
if ( ! function_exists( 'grenada_body_class' ) ){

	function grenada_body_class( $classes ) {

		$classes[] = 'hidden';

		global $grenada_theme_options;
		
		if( $grenada_theme_options['clapat_grenada_enable_smooth_scrolling'] ){
			
			$classes[] = 'smooth-scroll';
		}
		
		// return the $classes array
		return $classes;
	}
}
add_filter( 'body_class', 'grenada_body_class' );

// wp_head hook
if ( ! function_exists( 'grenada_wp_head_hook' ) ){

    function grenada_wp_head_hook(){

		global $grenada_theme_options;

		if( !empty( $grenada_theme_options['clapat_grenada_space_head'] ) ){
            echo wp_kses_post( $grenada_theme_options['clapat_grenada_space_head'] );
        }
    }

}
add_action('wp_head', 'grenada_wp_head_hook');

// site/blog title
if ( ! function_exists( '_wp_render_title_tag' ) ) {

	function grenada_wp_title() {

		echo wp_title( '|', false, 'right' );

	}
	add_action( 'wp_head', 'grenada_wp_title' );
}

if ( ! function_exists( 'grenada_menu_classes' ) ){

    function grenada_menu_classes(  $classes , $item, $args ){

		$classes[] = 'link';
		if( $item->menu_item_parent == "0" ){
			
			$classes[] = 'menu-timeline';
		}
		if( in_array( 'current-menu-item', $item->classes ) || in_array( 'current-menu-ancestor', $item->classes ) ){

			$classes[] = 'active';
		}
				
		return $classes;
    }

}
if ( ! function_exists( 'grenada_menu_link_attributes' ) ){

    function grenada_menu_link_attributes(  $atts, $item, $args ){

		$arr_classes = array();
		
		if( !empty($item->classes) ){
			if( !in_array( 'menu-item-type-custom', $item->classes ) && !in_array( 'menu-item-has-children', $item->classes ) ){
				
				// if the menu item is not a custom link
				$atts['data-type'] = 'page-transition';	
				$arr_classes[] = 'ajax-link';
			}
				
			$arr_classes[] = 'link';
		}
		
		if( !empty($item->classes) ){
			if( in_array( 'current-menu-item', $item->classes ) || in_array( 'current-menu-ancestor', $item->classes ) ){

				$arr_classes[] = 'active';
			}
		}

		if( !empty($item->classes) ){

			if( in_array( 'menu-item-has-children', $item->classes ) ){
				// if the menu item is a parent item, just use an empty <a> tag
				if( isset( $atts['data-type'] ) ){
					$atts['data-type'] = null;
				}
			}
		}
		if( !empty( $arr_classes ) ){

			$atts['class'] = implode( ' ', $arr_classes );
		}

		return $atts;
    }

}
// change priority here if there are more important actions associated with the hook
add_action('nav_menu_css_class', 'grenada_menu_classes', 10, 3);
add_filter('nav_menu_link_attributes', 'grenada_menu_link_attributes', 10, 3 );

// hooks to add extra classes for next & prev portfolio projects and single blog posts
if ( ! function_exists( 'grenada_prev_post_link' ) ){

    function grenada_prev_post_link( $output, $format, $link, $post ){

			if( $format == 'grenada_portfolio' ){

				$output = '';
				$next_post = $post;

				if( $post ){

					$next_post = $post;
				}
				else{ // could not find the next post so rewind to the oldest one

					$args = array(
							'posts_per_page'	=> 2,
							'order'           => 'DESC',
							'post_type'       => 'grenada_portfolio',
						);

					$find_query = new WP_Query( $args );
					if ( $find_query->have_posts() && ($find_query->found_posts > 1) )  {

						$find_query->the_post();

						$next_post = $find_query->post;

					} else {
						// no posts found
					}

					wp_reset_postdata();
				}

				if( $next_post ){

					global $grenada_theme_options;

					$grenada_hero_image	= grenada_get_post_meta( GRENADA_THEME_OPTIONS, $next_post->ID, 'grenada-opt-portfolio-hero-img' );
					$grenada_hero_title	= grenada_get_post_meta( GRENADA_THEME_OPTIONS, $next_post->ID, 'grenada-opt-portfolio-hero-caption-title' );
					
					$output = '<!-- Project Navigation --> ';
                    $output .= '<div id="project-nav">';
                    $output .= '<div class="next-project-wrap">';
                    $output .= '<div class="next-project-image" style="background-image:url(' . esc_url( $grenada_hero_image['url'] ) . ')"></div>';
                    $output .= '<a class="next-project-title next-ajax-link-project hide-ball " data-type="page-transition" href="'. esc_url( get_permalink( $next_post ) ) .'">';
                    $output .= '<div class="main-title">' .  wp_kses_post( $grenada_hero_title ) . '</div>';
                    $output .= '<div class="main-subtitle">' . wp_kses_post( $grenada_theme_options['clapat_grenada_portfolio_next_caption'] ) . '</div>';
                    $output .= '</a>';
                    $output .= '</div>';
                    $output .= '</div> ';     
                    $output .= '<!--/Project Navigation -->';

				}

			}
			else if(  $format == 'blog-post' ){

				$output = '';
				if( $post ){

					global $grenada_theme_options;

					$output = '<div class="post-navigation">';
                    $output .= '<div class="article-head">';
                    $output .= '<ul class="entry-meta">';
                    $output .= '<li>' .  $grenada_theme_options['clapat_grenada_blog_next_post_before']  . '</li>';
                    $output .= '</ul>';
                    $output .= '</div>';
                    $output .= '<div class="container">';
                    $output .= '<div class="post-next">';
                    $output .= '<a href="' . get_permalink( $post ) . '" class="ajax-link hide-ball" data-type="page-transition">' . $grenada_theme_options['clapat_grenada_blog_next_post_caption'] . '</a>';
                    $output .= '<div class="post-next-title">' . get_the_title( $post ) . '</div>';
                    $output .= '</div>';
                    $output .= '</div>';
                    $output .= '</div>';

				}

				return $output;
			}
			else {

				if( $post ){

					$output = get_permalink( $post );
				}
			}

			return $output;
    }

}
if ( ! function_exists( 'grenada_next_post_link' ) ){

    function grenada_next_post_link( $output, $format, $link, $post ){

			if( $format == 'grenada_portfolio' ){

				$output = '';
				$next_post = $post;

				if( $post ){

					$next_post = $post;
				}
				else{ // could not find the next post so rewind to the oldest one

					$args = array(
								'posts_per_page'   => 2,
								'order'            => 'ASC',
								'post_type'        => 'grenada_portfolio',
							);

					$find_query = new WP_Query( $args );
					if ( $find_query->have_posts() && ($find_query->found_posts > 1) )  {

						$find_query->the_post();

						$next_post = $find_query->post;

					} else {
						// no posts found
					}

					wp_reset_postdata();
				}

				if( $next_post ){

					global $grenada_theme_options;

					$grenada_hero_image = grenada_get_post_meta( GRENADA_THEME_OPTIONS, $next_post->ID, 'grenada-opt-portfolio-hero-img' );
					$grenada_hero_title	= grenada_get_post_meta( GRENADA_THEME_OPTIONS, $next_post->ID, 'grenada-opt-portfolio-hero-caption-title' );
					
					$output = '<!-- Project Navigation --> ';
                    $output .= '<div id="project-nav">';
                    $output .= '<div class="next-project-wrap">';
                    $output .= '<div class="next-project-image" style="background-image:url(' . esc_url( $grenada_hero_image['url'] ) . ')"></div>';
                    $output .= '<a class="next-project-title next-ajax-link-project hide-ball " data-type="page-transition" href="'. esc_url( get_permalink( $next_post ) ) .'">';
                    $output .= '<div class="main-title">' .  wp_kses_post( $grenada_hero_title ) . '</div>';
                    $output .= '<div class="main-subtitle">' . wp_kses_post( $grenada_theme_options['clapat_grenada_portfolio_next_caption'] ) . '</div>';
                    $output .= '</a>';
                    $output .= '</div>';
                    $output .= '</div> ';     
                    $output .= '<!--/Project Navigation -->';
				}

			}
			else if( $format == 'blog-post' ){

				// nothing here for the moment
			}
			else {

				if( $post ){

					$output = get_permalink( $post );
				}
			}

		return $output;
	}

}
// change priority here if there are more important actions associated with the hook
add_filter('next_post_link', 'grenada_next_post_link', 10, 4);
add_filter('previous_post_link', 'grenada_prev_post_link', 10, 4);

// hooks to add extra classes for next & prev blog posts
if ( ! function_exists( 'grenada_next_posts_link_attributes' ) ){

	function grenada_next_posts_link_attributes(){

		return 'class="ajax-link" data-type="page-transition"';
	}
}
if ( ! function_exists( 'grenada_prev_posts_link_attributes' ) ){

	function grenada_prev_posts_link_attributes(){

		return 'class="ajax-link" data-type="page-transition"';
	}
}
// change priority here if there are more important actions associated with the hook
add_filter('next_posts_link_attributes', 'grenada_next_posts_link_attributes', 10, 4);
add_filter('previous_posts_link_attributes', 'grenada_prev_posts_link_attributes', 10, 4);

if ( ! function_exists( 'grenada_comment_reply_link' ) ){

	function grenada_comment_reply_link($link, $args, $comment, $post){

		return str_replace("class='comment-reply-link", "class='comment-reply-link reply hide-ball", $link);
	}
}
// change priority here if there are more important actions associated with the hook
add_filter('comment_reply_link', 'grenada_comment_reply_link', 99, 4);

// category filter
if ( ! function_exists( 'grenada_category' ) ){
	
	function grenada_category( $thelist, $separator, $parents ){
		
		return str_replace('<a href="', '<a class="ajax-link link" data-type="page-transition" href="', $thelist);
	}
}
add_filter('the_category', 'grenada_category', 10, 3);

// tags filter
if ( ! function_exists( 'grenada_tags' ) ){
	
	function grenada_tags( $tag_list, $before, $sep, $after, $id ){
		
		return str_replace('<a href="', '<a class="ajax-link link" data-type="page-transition" href="', $tag_list);
	}
}
add_filter('the_tags', 'grenada_tags', 10, 5);

// search filter
if( !function_exists('grenada_searchfilter') ){

    function grenada_searchfilter( $query ) {

    	if ( !is_admin() && $query->is_main_query() ) {

    		if ($query->is_search ) {

    			$post_types = get_query_var('post_type');

    			if( empty( $post_types ) ){

            		$query->set('post_type', array('post'));
    			}
        	}

        }

        return $query;

    }
    add_filter('pre_get_posts','grenada_searchfilter');

}
