<?php

define('GRENADA_THEME_ID', 'grenada');
define('GRENADA_THEME_OPTIONS', 'clapat_' . GRENADA_THEME_ID . '_theme_options');

$grenada_social_links = array('Fb' => esc_html__('Facebook', 'grenada'),
							'Tw' => esc_html__('Twitter', 'grenada'),
							'Be' => esc_html__('Behance', 'grenada'),
							'Db' => esc_html__('Dribbble', 'grenada'),
							'Gp' => esc_html__('Google Plus', 'grenada'),
							'In' => esc_html__('Instagram', 'grenada'),
							'Ld' => esc_html__('Linkedin', 'grenada'),
							'Wa' => esc_html__('WhatsApp', 'grenada'),
							'Da' => esc_html__('DeviantArt', 'grenada'),
							'Dg' => esc_html__('Digg', 'grenada'),
							'Fr' => esc_html__('Flickr', 'grenada'),
							'Fq' => esc_html__('Foursquare', 'grenada'),
							'Gi' => esc_html__('Git', 'grenada'),
							'Pn' => esc_html__('Pinterest', 'grenada'),
							'Rd' => esc_html__('Reddit', 'grenada'),
							'Sk' => esc_html__('Skype', 'grenada'),
							'Sc' => esc_html__('Souncloud', 'grenada'),
							'Su' => esc_html__('Stumbleupon', 'grenada'),
							'Tb' => esc_html__('Tumblr', 'grenada'),
							'Ya' => esc_html__('Yahoo', 'grenada'),
							'Ye' => esc_html__('Yelp', 'grenada'),
							'Yt' => esc_html__('Youtube', 'grenada'),
							'Vm' => esc_html__('Vimeo', 'grenada'),
							'Xg'	=> esc_html__('Xing', 'grenada') );
							
define ('GRENADA_MAX_SOCIAL_LINKS', 8 );

?>