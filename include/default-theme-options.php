<?php

if ( !isset( $grenada_theme_options ) ){

	$grenada_theme_options = array();

}

if( !class_exists( 'ReduxFramework' ) ){

	$grenada_theme_options = array_merge ( $grenada_theme_options, array (
									
									"clapat_grenada_enable_ajax" => "1",
									"clapat_grenada_enable_smooth_scrolling" => "",
									"clapat_grenada_enable_preloader" => "1",
									"clapat_grenada_enable_fullscreen_menu" => "0",
									"clapat_grenada_enable_back_to_top" => "0",
									"clapat_grenada_space_head" => "",
									"clapat_grenada_logo" => array("url" => get_template_directory_uri() . "/images/logo.png"),
									"clapat_grenada_logo_light" => array("url" => get_template_directory_uri() . "/images/logo-white.png"),
									"clapat_grenada_footer_copyright_prefix" => esc_html__("Copyright ", "grenada"),
									"clapat_grenada_footer_copyright" => wp_kses_post( __('<p>2018 © <a target="_blank" href="https://www.clapat.com/themes/grenada/">Grenada Theme</a>.</p>', 'grenada') ),
									"clapat_grenada_footer_social_links_prefix" => esc_html__("Follow us on ", "grenada"),
									"clapat_grenada_footer_social_1" => "",
									"clapat_grenada_footer_social_url_1" => "",
									"clapat_grenada_footer_social_2" => "",
									"clapat_grenada_footer_social_url_2" => "",
									"clapat_grenada_footer_social_3" => "",
									"clapat_grenada_footer_social_url_3" => "",
									"clapat_grenada_footer_social_4" => "",
									"clapat_grenada_footer_social_url_4" => "",
									"clapat_grenada_footer_social_5" => "",
									"clapat_grenada_footer_social_url_5" => "",
									"clapat_grenada_footer_social_6" => "",
									"clapat_grenada_footer_social_url_6" => "",
									"clapat_grenada_footer_social_7" => "",
									"clapat_grenada_footer_social_url_7" => "",
									"clapat_grenada_footer_social_8" => "",
									"clapat_grenada_footer_social_url_8" => "",
									"clapat_grenada_portfolio_custom_slug" => "",
									"clapat_grenada_portfolio_next_caption" => esc_html__("next", "grenada"),
									"clapat_grenada_portfolio_navigation_order" => "forward",
									"clapat_grenada_blog_posted_by_caption" => esc_html__("Posted By", "grenada"),
									"clapat_grenada_blog_next_post_before" => esc_html__("Read Also", "grenada"),
									"clapat_grenada_blog_next_post_caption" => esc_html__("Next", "grenada"),
									"clapat_grenada_blog_default_title" => esc_html__("Blog", "grenada"),
									"clapat_grenada_map_marker" => array("url" => get_template_directory_uri() . "/images/marker.png"),
									"clapat_grenada_map_address" => "43.270441,6.640888",
									"clapat_grenada_map_zoom" => "14",
									"clapat_grenada_map_company_name" => esc_html__("Hello Friend!", "grenada"),
									"clapat_grenada_map_company_info" => esc_html__("Here we are. Come to drink a coffee!", "grenada"),
									"clapat_grenada_map_type" => "1",
									"clapat_grenada_map_api_key" => "",
									"clapat_grenada_error_title" => esc_html__("404", "grenada"),
									"clapat_grenada_error_info" => esc_html__("The page you are looking for could not be found.", "grenada"),
									"clapat_grenada_error_back_button" => esc_html__("Home Page", "grenada"),
									"clapat_grenada_error_back_button_url" => get_home_url(),					
									) );
}

if( !class_exists( 'ReduxFramework' ) || !function_exists('grenada_redux_register_custom_extension_loader') ){

	$grenada_theme_options = array_merge ( $grenada_theme_options, array (
									"grenada-opt-page-bknd-color" =>esc_attr("dark-content"), 
									"grenada-opt-page-enable-hero" =>"", 
									"grenada-opt-page-hero-caption-title" =>"", 
									"grenada-opt-page-hero-caption-subtitle" =>"", 
									"grenada-opt-page-text-alignment" =>esc_attr("text-align-left"), 
									"grenada-opt-page-showcase-layout" => esc_attr("split-slider"),
									"grenada-opt-blog-bknd-color" =>esc_attr("dark-content"),
									"grenada-opt-blog-text-alignment" =>esc_attr("text-align-center"), 
									"grenada-opt-portfolio-bknd-color" =>esc_attr("dark-content"),
									"grenada-opt-portfolio-hero-img" =>"",
									"grenada-opt-portfolio-video" => "",
									"grenada-opt-portfolio-video-mp4" => "",
									"grenada-opt-portfolio-video-webm" => "",
									"grenada-opt-portfolio-hero-caption-title" =>"", 
									"grenada-opt-portfolio-hero-caption-subtitle" =>"", 
									"grenada-opt-portfolio-hero-position" =>esc_attr("fixed-onscroll"), 
								) );
}

?>
