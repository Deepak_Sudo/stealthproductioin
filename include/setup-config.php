<?php
/**
 * Created by Clapat.
 * Date: 28/03/16
 * Time: 6:21 AM
 */

// register navigation menus
if ( ! function_exists( 'grenada_register_nav_menus' ) ){
	
	function grenada_register_nav_menus() {
		
	  register_nav_menus(
		array(
		  'primary-menu' => esc_html__( 'Primary Menu', 'grenada')
		)
	  );
	}
}
add_action( 'init', 'grenada_register_nav_menus' );
 
// custom excerpt length
if( !function_exists('grenada_custom_excerpt_length') ){

    function grenada_custom_excerpt_length( $length ) {

        global $grenada_theme_options;

        return intval( $grenada_theme_options['clapat_grenada_blog_excerpt_length'] );
    }
}

// theme setup
if ( ! function_exists( 'grenada_theme_setup' ) ){

    function grenada_theme_setup() {

        // Set content width
        if ( ! isset( $content_width ) ) $content_width = 1180;

        // add support for multiple languages
        load_theme_textdomain( 'grenada', get_template_directory() . '/languages' );
			
	}

} // grenada_theme_setup

/**
 * Tell WordPress to run grenada_theme_setup() when the 'after_setup_theme' hook is run.
 */
add_action( 'after_setup_theme', 'grenada_theme_setup' );