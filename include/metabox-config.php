<?php

// You may replace $redux_opt_name with a string if you wish. If you do so, change loader.php
// as well as all the instances below.
$redux_opt_name = 'clapat_' . GRENADA_THEME_ID . '_theme_options';


if ( !function_exists( "grenada_add_metaboxes" ) ){

    function grenada_add_metaboxes( $metaboxes ) {

    $metaboxes = array();


    ////////////// Page Options //////////////
    $page_options = array();
    $page_options[] = array(
        'title'         => esc_html__('General', 'grenada'),
        'icon_class'    => 'icon-large',
        'icon'          => 'el-icon-wrench',
        'desc'          => esc_html__('Options concerning all page templates.', 'grenada'),
        'fields'        => array(
			
			array(
                'id'        => 'grenada-opt-page-bknd-color',
                'type'      => 'select',
                'title'     => esc_html__('Background color', 'grenada'),
				'desc'      => esc_html__('Background color for this page.', 'grenada'),
                'options'   => array(
                    'dark-content' 	=> esc_html__('White', 'grenada'),
                    'light-content' => esc_html__('Black', 'grenada')

                ),
				'default'   => 'dark-content',
            ),
			
			/**************************HERO SECTION OPTIONS**************************/
			array(
                'id'        => 'grenada-opt-page-enable-hero',
                'type'      => 'switch',
                'title'     => esc_html__('Display Hero Section', 'grenada'),
                'desc'      => esc_html__('Enable "hero" section displayed immediately below page header. Showcase pages do not have a hero section.', 'grenada'),
				'on'       => esc_html__('Yes', 'grenada'),
				'off'      => esc_html__('No', 'grenada'),
                'default'   => false
            ),

			array(
                'id'        => 'grenada-opt-page-hero-caption-title',
                'type'      => 'text',
				'required'  => array( 'grenada-opt-page-enable-hero', '=', true ),
                'title'     => esc_html__('Hero Caption Title', 'grenada'),
                'subtitle'  => esc_html__('Caption title displayed over hero section.', 'grenada'),
	        ),

			array(
                'id'        => 'grenada-opt-page-hero-caption-subtitle',
                'type'      => 'textarea',
				'required'  => array( 'grenada-opt-page-enable-hero', '=', true ),
                'title'     => esc_html__('Hero Caption Subtitle', 'grenada'),
                'subtitle'  => esc_html__('Caption subtitle displayed over hero section. Enter plain text.', 'grenada'),
                'validate'  => 'html', //see http://codex.wordpress.org/Function_Reference/wp_kses_post
	        ),

			array(
                      'id'        => 'grenada-opt-page-text-alignment',
                      'type'      => 'select',
                      'title'     => esc_html__('Hero Text Alignment', 'grenada'),
                      'desc'      => esc_html__('Position of the "hero" caption.', 'grenada'),
					  'options'   => array(
                          'text-align-left' 	=> esc_html__('Left Justified', 'grenada'),
                          'text-align-center' 	=> esc_html__('Centered', 'grenada'),
                      ),
                      'default'   => 'text-align-center',
					  'required'  => array( 'grenada-opt-page-enable-hero', '=', true ),
            ),

			/**************************END - HERO SECTION OPTIONS**************************/
        ),
    );

	$page_options[] = array(
        'title'         => esc_html__('Portfolio and Portfolio Mixed Templates', 'grenada'),
        'icon_class'    => 'icon-large',
        'icon'          => 'el-icon-folder-open',
        'desc'          => esc_html__('Options concerning only Portfolio templates.', 'grenada'),
        'fields'        => array(

			array(
                'id'        => 'grenada-opt-page-portfolio-hover-effect',
                'type'      => 'select',
                'title'     => esc_html__('Hover Effect', 'grenada'),
				'desc'      => esc_html__('The type of hover for each portfolio item thumbnail.', 'grenada'),
                'options'   => array(
                    'below-caption' 	=> esc_html__('Caption Below', 'grenada'),
                ),
				'default'   => 'below-caption'
            ),
			
			array(
                'id'        => 'grenada-opt-page-portfolio-layout',
                'type'      => 'select',
                'title'     => esc_html__('Page Layout', 'grenada'),
				'desc'      => esc_html__('The portfolio page layout.', 'grenada'),
                'options'   => array(
                    'wide' 	=> esc_html__('Wide', 'grenada'),
					'boxed'	=> esc_html__('Boxed', 'grenada'),
                ),
				'default'   => 'wide'
            ),
			
			array(
                 'id'       => 'grenada-opt-page-portfolio-mixed-items',
                 'type'     => 'text',
                 'title'    => esc_html__( 'Maximum Number Of Items In Portfolio Mixed', 'grenada' ),
                 'desc' => esc_html__( 'Available only for Portfolio Mixed Template: the maximum number of portfolio items displayed. Leave empty for ALL.', 'grenada' )
             ),
			 
			 array(
                 'id'       => 'grenada-opt-page-portfolio-mixed-content-position',
                 'type'     => 'select',
                 'title'    => esc_html__( 'Page Content Position', 'grenada'),
                 'desc' => esc_html__( 'Available only for Portfolio Mixed Template: page content position in relation with portfolio grid.', 'grenada'),
                 'options'   => array(
                    'after' 	=> esc_html__('After Portfolio Grid', 'grenada'),
					'before'	=> esc_html__('Before Portfolio Grid', 'grenada'),
                 ),
				 'default'	=> true,
            ),
			
		),
	);
		
	$page_options[] = array(
        'title'         => esc_html__('Showcase Template', 'grenada'),
        'icon_class'    => 'icon-large',
        'icon'          => 'el-icon-folder-open',
        'desc'          => esc_html__('Options concerning only Showcase template.', 'grenada'),
        'fields'        => array(

			array(
                'id'        => 'grenada-opt-page-showcase-layout',
                'type'      => 'select',
                'title'     => esc_html__('Showcase Layout', 'grenada'),
				'desc'      => esc_html__('Type of the layout for the showcase slider.', 'grenada'),
                'options'   => array(
                    'split-slider' 			=> esc_html__('Split', 'grenada'),
                    'centered-slider' 	=> esc_html__('Centered', 'grenada'),
					'fullscreen-slider'	=> esc_html__('Fullscreen', 'grenada')
                ),
				'default'   => 'split-slider'
            ),
			
        ),

    );

	$metaboxes[] = array(
        'id'            => 'clapat_' . GRENADA_THEME_ID . '_page_options',
        'title'         => esc_html__( 'Page Options', 'grenada'),
        'post_types'    => array( 'page' ),
        'position'      => 'normal', // normal, advanced, side
        'priority'      => 'high', // high, core, default, low
        'sidebar'       => false, // enable/disable the sidsebar in the normal/advanced positions
        'sections'      => $page_options,
    );

    $blog_post_options = array();
    $blog_post_options[] = array(

         'icon_class'    => 'icon-large',
         'icon'          => 'el-icon-wrench',
         'fields'        => array(

			array(
                'id'        => 'grenada-opt-blog-bknd-color',
                'type'      => 'select',
                'title'     => esc_html__('Background color', 'grenada'),
				'desc'      => esc_html__('Background color for this blog post.', 'grenada'),
                'options'   => array(
                    'dark-content' 	=> esc_html__('White', 'grenada'),
                    'light-content' => esc_html__('Black', 'grenada')

                ),
				'default'   => 'dark-content',
            ),
			
			array(
                      'id'        => 'grenada-opt-blog-text-alignment',
                      'type'      => 'select',
                      'title'     => esc_html__('Hero Text Alignment', 'grenada'),
                      'desc'      => esc_html__('Position of the "hero" caption. The hero section is displayed at the top of the blog post and displays post title and categories.', 'grenada'),
						'options'   => array(
                          'text-align-left' 	=> esc_html__('Left Justified', 'grenada'),
                          'text-align-center' 	=> esc_html__('Centered', 'grenada'),
                      ),
                      'default'   => 'text-align-center',
                  ),
				  
          )
        );
			/**************************END - HERO SECTION OPTIONS**************************/

    $metaboxes[] = array(
       'id'            => 'clapat_' . GRENADA_THEME_ID . '_post_options',
       'title'         => esc_html__( 'Post Options', 'grenada'),
       'post_types'    => array( 'post' ),
       'position'      => 'normal', // normal, advanced, side
       'priority'      => 'high', // high, core, default, low
       'sidebar'       => false, // enable/disable the sidebar in the normal/advanced positions
       'sections'      => $blog_post_options,
    );


    $portfolio_options = array();
    $portfolio_options[] = array(

        'icon_class'    => 'icon-large',
        'icon'          => 'el-icon-wrench',
        'fields'        => array(

			array(
                'id'        => 'grenada-opt-portfolio-bknd-color',
                'type'      => 'select',
                'title'     => esc_html__('Background color', 'grenada'),
				'desc'      => esc_html__('Background color for this page.', 'grenada'),
                'options'   => array(
                    'dark-content' 	=> esc_html__('White', 'grenada'),
                    'light-content' => esc_html__('Black', 'grenada')

                ),
				'default'   => 'dark-content',
            ),
			
			array(
                'id'        => 'grenada-opt-portfolio-showcase-include',
                'type'      => 'select',
                'title'     => esc_html__('Include In Showcase Slider', 'grenada'),
                'desc'      => esc_html__('Include this portfolio item in the Showcase slider. The slider is displayed in Showcase page template.', 'grenada'),
				'options'   => array(
                    'yes'		=> esc_html__('Include in Showcase', 'grenada'),
                    'no'  	=> esc_html__('Exclude from Showcase', 'grenada')

                ),
                'default'   => 'yes'
            ),
			
			array(
                'id'        => 'grenada-opt-portfolio-thumbnail-size',
                'type'      => 'select',
                'title'     => esc_html__('Thumbnail Size', 'grenada'),
                'desc'      => esc_html__('Size of the thumbnail for this item as it appears in portfolio template pages. The thumbnail image is the hero image assigned for this item.', 'grenada'),
				'options'   => array(
                    'normal' => esc_html__('Normal', 'grenada'),
                    'wide' => esc_html__('Wide', 'grenada')
                ),
                'default'   => 'normal'
            ),
			
			/**************************HERO SECTION OPTIONS**************************/
			array(
				'id'        => 'grenada-opt-portfolio-hero-img',
                'type'      => 'media',
                'url'       => true,
                'title'     => esc_html__('Hero Image', 'grenada'),
                'desc'      => esc_html__('Upload hero background image.  The hero image is being displayed in portfolio showcase. Hero section is the header section displayed at the top of the project page.', 'grenada'),
                'default'   => array(),
            ),
			
			array(
                'id'        => 'grenada-opt-portfolio-video',
                'type'      => 'switch',
				'title'     => esc_html__('Video Hero', 'grenada'),
				'desc'   	=> esc_html__('Video displayed as hero section and showcase slide. If you check this option set the Hero Image as the first frame image of the video to avoid flickering!', 'grenada'),
                'on'       => esc_html__('Yes', 'grenada'),
				'off'      => esc_html__('No', 'grenada'),
                'default'   => false
            ),
			
			array(
                'id'        => 'grenada-opt-portfolio-video-webm',
                'type'      => 'text',
                'title'     => esc_html__('Webm Video URL', 'grenada'),
                'desc'   	=> esc_html__('URL of the showcase slide background webm video. Webm format is previewed in Chrome and Firefox.', 'grenada'),
				'required'	=> array('grenada-opt-portfolio-video', '=', true),
            ),
			
			array(
                'id'        => 'grenada-opt-portfolio-video-mp4',
                'type'      => 'text',
                'title'     => esc_html__('MP4 Video URL', 'grenada'),
                'desc'   	=> esc_html__('URL of the showcase slide background MP4 video. MP4 format is previewed in IE, Safari and other browsers.', 'grenada'),
                'required'	=> array('grenada-opt-portfolio-video', '=', true),
            ),
						
			array(
                'id'        => 'grenada-opt-portfolio-hero-caption-title',
                'type'      => 'text',
				'title'     => esc_html__('Hero Caption Title', 'grenada'),
                'subtitle'  => esc_html__('Caption title displayed over hero section. The hero background image is set in the hero image set in preceding option.', 'grenada'),
	        ),

			array(
                'id'        => 'grenada-opt-portfolio-hero-caption-subtitle',
                'type'      => 'textarea',
				'title'     => esc_html__('Hero Caption Subtitle', 'grenada'),
                'subtitle'  => esc_html__('Caption subtitle displayed over hero section. Enter plain text.', 'grenada'),
                'validate'  => 'html', //see http://codex.wordpress.org/Function_Reference/wp_kses_post
	        ),
			
			array(
                'id'        => 'grenada-opt-portfolio-hero-position',
                'type'      => 'select',
                'title'     => esc_html__('Hero Position', 'grenada'),
                'desc'      => esc_html__('Position of the "hero" section displayed as page header.', 'grenada'),
				'options'   => array(
                    'fixed-onscroll' 	=> esc_html__('Relative', 'grenada'),
                    'parallax-onscroll' => esc_html__('Parallax', 'grenada')
                ),
                'default'   => 'fixed-onscroll'
            ),
			/**************************END - HERO SECTION OPTIONS**************************/

        ),
    );

    $metaboxes[] = array(
        'id'            => 'clapat_' . GRENADA_THEME_ID . '_portfolio_options',
        'title'         => esc_html__( 'Portfolio Item Options', 'grenada'),
        'post_types'    => array( 'grenada_portfolio' ),
        'position'      => 'normal', // normal, advanced, side
        'priority'      => 'high', // high, core, default, low
        'sidebar'       => false, // enable/disable the sidebar in the normal/advanced positions
        'sections'      => $portfolio_options,
    );

	return $metaboxes;
  }

  add_action('redux/metaboxes/'.$redux_opt_name.'/boxes', 'grenada_add_metaboxes');
  
}

if( class_exists('Clapat\Grenada\Metaboxes\Meta_Boxes') ){

	$metabox_definitions = array();
	$metabox_definitions = grenada_add_metaboxes( $metabox_definitions );
	do_action( 'clapat/grenada/add_metaboxes', $metabox_definitions );
}