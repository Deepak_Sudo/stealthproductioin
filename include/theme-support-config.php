<?php
/**
 * Created by Clapat.
 * Date: 17/09/15
 * Time: 5:51 AM
 */

// Featured images support
add_theme_support( 'post-thumbnails', array( 'post', 'grenada_portfolio' ) ); 
// Automatic feed links support
add_theme_support( 'automatic-feed-links' );
// title
add_theme_support( 'title-tag' );
// image wide or full alignment
add_theme_support( 'align-wide' );
// support for responsive embeds
add_theme_support( 'responsive-embeds' );

?>