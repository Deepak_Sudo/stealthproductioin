<?php

get_header();

while ( have_posts() ){

    the_post();

	$grenada_post_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
	$grenada_text_alignment = grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-blog-text-alignment' );
?>
	
	<!-- Main -->
	<div id="main">
			
			<!-- Hero Section -->
            <div id="hero" class="post-hero">                    
				<div id="hero-caption" class="<?php echo sanitize_html_class( $grenada_text_alignment ); ?>">
					<div class="inner">
						<div class="post-article-wrap">                            
							<div class="article-head">
								<ul class="entry-meta entry-date">
									<li><a class="link" href="<?php the_permalink(); ?>"><?php the_time('F j, Y'); ?></a></li>
								</ul>
							</div>
                                                    
                            <div class="article-content">                                
								<div class="post-title"><?php the_title(); ?></div>                                
								<div class="entry-meta entry-categories">
									<?php the_category(); ?>                               
								</div>
                            </div>                                                                                        
						</div>
                    </div>
                </div>
            </div>                     
            <!--/Hero Section -->   
				
		<!-- Main Content -->
		<div id="main-content" >
			<!-- Post -->
			<div id="post" <?php post_class('post-content'); ?>>
				<!-- Post Content -->
				<div id="post-content">
					
					<div class="article-head">                            
						<ul class="entry-meta entry-author">
							<li>
								<?php echo wp_kses_post( grenada_get_theme_options('clapat_grenada_blog_posted_by_caption') ); ?> <br class="destroy">
								<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" class="link"> 
									<?php the_author(); ?>
								</a>
							</li>
                        </ul>
					</div>
					
					<?php if( $grenada_post_image ){ ?>
					<div class="post-image">
						<img src="<?php echo esc_url( $grenada_post_image[0] ); ?>" alt="<?php esc_attr_e("Post Image", "grenada"); ?>">
					</div>
					<?php } ?>
					
					<?php the_content(); ?>
								
					<div class="page-links">
					<?php
						wp_link_pages();
					?>
					</div>
					
				</div>
				<!--/Post Content -->
				
				<!-- Post Meta Data -->
                <div class="post-meta-data">
					<div class="container">
						<?php if ( has_tag() ) {
							the_tags( '<ul class="entry-meta entry-tags"><li>' . esc_html__('Tags:', 'grenada') .'</li><li>', '</li><li>', '</li></ul>' );
						} ?>
                    </div>
                </div>
                <!--/Post Meta Data -->
				
				<!-- Post Navigation -->
				<?php previous_post_link( 'blog-post', grenada_get_theme_options( 'clapat_grenada_blog_next_post_caption' ) ); ?>
				<!--/Post Navigation -->
				
				<?php comments_template(); ?>
				
			</div>
			<!-- /Post -->
		</div>
		<!-- /Main Content -->
	</div>
	<!-- /Main -->
<?php

}

get_footer();

?>
