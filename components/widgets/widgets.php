<?php

// more widgets in the (near) future...

// Register widgetized locations
if(  !function_exists('grenada_widgets_init') ){

    function grenada_widgets_init(){

		$args = array( 'name'          	=> __( 'Blog Sidebar', 'grenada' ),
								'id'            		=> 'grenada-blog-sidebar',
								'description'   	=> '',
								'class'         		=> '',
								'before_widget' => '<div id="%1$s" class="widget grenada-sidebar-widget %2$s">',
								'after_widget'  	=> '</div>',
								'before_title'  	=> '<h5 class="widgettitle grenada-widgettitle">',
								'after_title'   		=> '</h5>' );
		
		register_sidebar( $args );
        
    }
}

add_action( 'widgets_init', 'grenada_widgets_init' );

?>