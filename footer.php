<?php
/**
 * Created by Clapat.
 * Date: 14/08/18
 * Time: 11:14 AM
 */
?>
			<?php
				
			// display footer section
			get_template_part('sections/footer_section'); 
				
			?>
			</div>
			<!--/Page Content -->
		</div>
		<!--/Cd-main-content -->
	</main>
	<!--/Main -->	
	
	<div class="cd-cover-layer"></div>
    <div id="magic-cursor">
        <div id="ball">
        	<div id="ball-loader"></div>
        </div>
    </div>
	<div id="clone-image"></div>
    <div id="rotate-device"></div>
<?php wp_footer(); ?>
</body>
</html>
