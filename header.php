<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<main>
    <?php if( grenada_get_theme_options('clapat_grenada_enable_preloader') ){ ?>
		<!-- Preloader -->
        <div class="preloader-wrap">
            <div class="outer">
                <div class="inner">
                    <div class="percentage" id="precent"></div>                          
                </div>
            </div>
        </div>
        <!--/Preloader -->  
  <?php } ?>
		
		<!--Cd-main-content -->
		<div class="cd-index cd-main-content">
			
		<?php
		$grenada_bknd_color = 'dark-content';
		if( is_singular( 'grenada_portfolio' ) ){
	
			$grenada_bknd_color = grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-portfolio-bknd-color' );
			
		}
		else if( is_singular( 'post' ) ){
	
			$grenada_bknd_color = grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-blog-bknd-color' );
			
		}
		else if( is_tax('portfolio_category') ){
			
			$grenada_bknd_color = 'light-content';
			
		}
		else if( is_page() ){
	
			$grenada_bknd_color = grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-page-bknd-color' );

		}
		?>
	
		<?php if( is_page_template( 'blog-page.php' ) || is_home() || is_archive() || is_search() ){ ?>
			<!-- Page Content -->
			<div id="page-content" class="blog-template <?php echo sanitize_html_class( $grenada_bknd_color ); ?>">
		<?php } else { ?>
			<!-- Page Content -->
			<div id="page-content" class="<?php echo sanitize_html_class( $grenada_bknd_color ); ?>">
		<?php } ?>
		
			<?php 
				// display header section
				get_template_part('sections/header_section'); 		
			?>