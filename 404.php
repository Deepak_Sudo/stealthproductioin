<?php 

get_header(); 

?>
	
		<!-- Hero --> 
        <div id="hero" class="error">
			<div id="hero-styles">
				<!-- Hero Caption -->
                <div id="hero-caption">
					<div class="inner text-align-center">
						<div class="404caption">
							<h1 class="hero-title"><span><?php echo wp_kses( grenada_get_theme_options('clapat_grenada_error_title'), wp_kses_allowed_html( 'post' ) ); ?></span></h1>
							<div class="hero-subtitle">
                                <p><?php echo wp_kses_post ( grenada_get_theme_options('clapat_grenada_error_info') ); ?></p><br>
                                <a class="clapat-button hide-ball ajax-link" href="<?php echo esc_url( grenada_get_theme_options('clapat_grenada_error_back_button_url') ); ?>" data-type="page-transition">
                                    <span><?php echo wp_kses_post( grenada_get_theme_options('clapat_grenada_error_back_button') ); ?></span>
                                </a>
                            </div>
						</div>
		            </div>
                </div>
                <!--/Hero Caption -->
			</div>
		</div>
		<!-- /Hero --> 

<?php 

get_footer(); 

?>