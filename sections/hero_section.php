<?php
/**
 * Created by Clapat.
 * Date: 28/08/18
 * Time: 11:33 AM
 */

// hero section container properties
$grenada_hero_properties = grenada_get_hero_properties( get_post_type() );

if( $grenada_hero_properties->enabled ){

	get_template_part('sections/hero_section_container');
}
else {
?>

		<!-- Hero Section -->
        <div id="hero">
           <div id="hero-styles" class="parallax-onscroll">
                <div id="hero-caption" class="">
                    <div class="inner">
                        <h1 class="hero-title"><?php the_title(); ?></h1> 
                    </div>
                </div>                    
            </div>
        </div>                      
        <!--/Hero Section -->

<?php
}

?>
