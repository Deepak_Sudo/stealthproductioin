<?php
/**
 * Created by Clapat.
 * Date: 02/10/18
 * Time: 1:34 PM
 */
$grenada_hero_properties = grenada_get_hero_properties( get_post_type() );

$hero_styles = $grenada_hero_properties->position;

if( $grenada_hero_properties->enabled ){

?>

		<!-- Hero Section -->
		<div id="hero1" class="<?php if( $grenada_hero_properties->image && !empty( $grenada_hero_properties->image['url'] ) ){ echo 'has-image'; } ?>">
				<div id="hero-styles" class="<?php echo esc_attr( $hero_styles ); ?>">
						<div id="hero-caption" class="<?php echo sanitize_html_class( $grenada_hero_properties->text_alignment ); ?>">
								<div class="inner">
										<h1 class="hero-title"><?php echo wp_kses_post( $grenada_hero_properties->caption_title ); ?></h1>
										<h2 class="hero-subtitle"><?php echo wp_kses_post( $grenada_hero_properties->caption_subtitle ); ?></h2>
								</div>
								<?php if( $grenada_hero_properties->image && !empty( $grenada_hero_properties->image['url'] ) ){ ?>
								<div class="scroll-down-wrap no-border link">
                                     <div class="section-down-arrow">
                                        <svg class="nectar-scroll-icon" viewBox="0 0 30 45" enable-background="new 0 0 30 45">
                                            <path class="nectar-scroll-icon-path" fill="none" stroke="#ffffff" stroke-width="2" stroke-miterlimit="10" d="M15,1.118c12.352,0,13.967,12.88,13.967,12.88v18.76  c0,0-1.514,11.204-13.967,11.204S0.931,32.966,0.931,32.966V14.05C0.931,14.05,2.648,1.118,15,1.118z">
                                            </path>
                                        </svg>
                                    </div>
                                </div> 
								<?php } ?>
						</div>
				</div>
		</div>
		<?php if( $grenada_hero_properties->image && !empty( $grenada_hero_properties->image['url'] ) ){ ?>
		<div id="hero-bg-wrapper">
			<div id="hero-image-parallax">
				<div id="hero-bg-image" class="<?php echo sanitize_html_class( $grenada_hero_properties->foreground ); ?>" style="background-image:url(<?php echo esc_url( $grenada_hero_properties->image['url'] ); ?>)">
				<?php if( $grenada_hero_properties->video ){ ?>
					<div class="hero-video-wrapper">
						<video loop muted class="bgvid">
							<?php if( !empty( $grenada_hero_properties->video_mp4 ) ){ ?>
							<source src="<?php echo esc_url( $grenada_hero_properties->video_mp4 ); ?>" type="video/mp4">
							<?php } ?>
							<?php if( !empty( $grenada_hero_properties->video_webm ) ){ ?>
							<source src="<?php echo esc_url( $grenada_hero_properties->video_webm ); ?>" type="video/webm">
							<?php } ?>
						</video>
					 </div>
				<?php } ?>
				</div>
			</div>
		</div>
		<?php } ?>
		<!--/Hero Section -->

<?php
}
?>
