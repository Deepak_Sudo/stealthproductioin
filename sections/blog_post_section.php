<?php
$grenada_post_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
?>
				<!-- Article -->
                <article id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>
					<div class="article-wrap">                            
						<div class="article-head">
                            <ul class="entry-meta entry-date">
								<li><a class="link" href="<?php echo esc_url( the_permalink() ); ?>"><?php the_time('F j, Y'); ?></a></li>
                            </ul>
                        </div>
                        <?php if( $grenada_post_image ){ ?>
						<div class="article-img" style="background-image: url(<?php echo esc_url( $grenada_post_image[0] ); ?>)"></div>
						<?php } ?>
                        <div class="article-content">                                
							<a class="post-title <?php if( !$grenada_post_image ){ echo "hide-ball"; } ?>" href="<?php echo esc_url( the_permalink() ); ?>" data-type="page-transition"><?php the_title(); ?></a>                            <div class="entry-meta entry-categories">
								<?php the_category(); ?>                               
                            </div>
							<div class="page-links">
							<?php
								wp_link_pages();
							?>
							</div>
                        </div>                                                                                        
                     </div>                
                </article>
                <!--/Article -->