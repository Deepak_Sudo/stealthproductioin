			<!-- Footer -->
			<?php if( is_page_template('showcase-page.php') ){ ?>
			</div>
			
			<footer class="fixed">
			<?php } else { ?>
			<footer class="hidden">
			<?php } ?>
                <div id="footer-container">
                	
					<?php if( is_page_template('showcase-page.php') ){
						
						$grenada_page_container_list = grenada_showcase_slides();
						$grenada_slides_count = 1;
						
						echo '<div id="counter-wrap" data-hover="' . sprintf("%02d", esc_attr( count( $grenada_page_container_list ) ) ) . '">';
                 
						foreach( $grenada_page_container_list as $grenada_page_container_item ){
							
							echo '<span data-slide-count="' . esc_attr( $grenada_slides_count ) . '">' . sprintf("%02d", esc_attr( $grenada_slides_count ) ) . '</span>';
							$grenada_slides_count++;
						}
						
						echo '</div>';
					
					} else {
						
						get_template_part('sections/footer_info');
					}
					?>
                    <?php if( grenada_get_theme_options( 'clapat_grenada_enable_back_to_top' ) ){ ?>
                    <div id="backtotop">
                    	<div class="parallax-wrap">
                        	<div class="parallax-element"><i class="fa fa-angle-up"></i></div>
                        </div>
                    </div>
					<?php } ?>
					<?php get_template_part('sections/footer_social_links_section'); ?>
                                        
                </div>
            </footer>
            <!--/Footer -->
			
			<?php if( !is_page_template('showcase-page.php') ){ ?>
			</div>
			<?php } ?>
			
			<?php if( is_page_template('portfolio-page.php') || is_page_template('portfolio-mixed-page.php') || is_tax('portfolio_category') ){ ?>
			<div class="thumb-container">
				<?php
					$grenada_page_container_list = grenada_portfolio_images();
					foreach( $grenada_page_container_list as $grenada_page_container_item ){
						echo '<div class="thumb-page" data-src="' . esc_url( $grenada_page_container_item ) . '"></div>';
					}
				?>
            </div>
			<?php } ?>