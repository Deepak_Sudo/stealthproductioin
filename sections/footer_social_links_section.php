<?php

if( !function_exists('grenada_render_footer_social_links' ) )
{
	function grenada_render_footer_social_links(){

		global $grenada_theme_options;
		global $grenada_social_links;

		$grenada_social_links = "";
		for( $idx = 1; $idx <= GRENADA_MAX_SOCIAL_LINKS; $idx++ ){

			$social_name = $grenada_theme_options['clapat_grenada_footer_social_' . $idx];
			$social_url  = $grenada_theme_options['clapat_grenada_footer_social_url_' . $idx];

			if( $social_url ){

				$grenada_social_links .= '<li><span class="parallax-wrap"><a class="parallax-element" href="' . esc_url( $social_url ) . '" target="_blank">' . wp_kses_post( $social_name ) . '</a></span></li>';

			}

		}
		
		if( !empty( $grenada_social_links ) ){
?>
						<div class="socials-wrap">            	
							<div class="socials-icon"><i class="fa fa-share-alt" aria-hidden="true"></i></div>
							<div class="socials-text"><?php echo wp_kses_post( grenada_get_theme_options('clapat_grenada_footer_social_links_prefix') ); ?></div>
							<ul class="socials">
								<?php echo wp_kses_post( $grenada_social_links ); ?>
							</ul>
						</div>
<?php			
		
		}

	}
}

grenada_render_footer_social_links();

?>
