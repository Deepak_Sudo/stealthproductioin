 					<div class="copyright-wrap">
						<?php if( grenada_get_theme_options('clapat_grenada_footer_copyright') ){ ?>
                        <div class="copyright-icon"><i class="fa fa-copyright" aria-hidden="true"></i></div>
                        <div class="copyright-text"><?php echo wp_kses_post( grenada_get_theme_options('clapat_grenada_footer_copyright_prefix') ); ?></div>
						
						<div class="copyright">
                            <?php echo wp_kses_post( grenada_get_theme_options('clapat_grenada_footer_copyright') ); ?>
                        </div>
						<?php
						}
						?>
                    </div>
