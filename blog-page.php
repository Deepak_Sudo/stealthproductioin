<?php
/*
Template name: Blog Template
*/
get_header();

while ( have_posts() ){

the_post();

?>
			
	<!-- Main -->
	<div id="main">
	<?php 
		
		// display hero section, if any
		get_template_part('sections/hero_section'); 
		
	?>
		<!-- Main Content -->
    	<div id="main-content">
			<!-- Blog-->
			<div id="blog">
				<!-- Blog-Content-->
				<div id="blog-content">
				<?php 
						
					$grenada_paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
					
					$grenada_args = array(
						'post_type' => 'post',
						'paged' => $grenada_paged
					);
					$grenada_posts_query = new WP_Query( $grenada_args );

					// the loop
					while( $grenada_posts_query->have_posts() ){

						$grenada_posts_query->the_post();

						get_template_part( 'sections/blog_post_section' );
						
					}
							
				?>
				</div>
				<!-- /Blog-Content -->
					
				<?php
						
					grenada_pagination( $grenada_posts_query );

					wp_reset_postdata();
				?>
			</div>
			<!-- /Blog-->
		</div>
		<!--/Main Content-->
	</div>
	<!-- /Main -->
<?php

}
	
get_footer();

?>
