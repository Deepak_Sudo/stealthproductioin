<?php


require_once ( get_template_directory() . '/include/defines.php' );

// Admin framework
if( class_exists( 'ReduxFramework' ) ){

	// Metaboxes
	require_once ( get_template_directory() . '/include/metabox-config.php');

	// Admin theme options
	if ( !isset( $grenada_theme_options ) && file_exists( get_template_directory() . '/include/admin-config.php' ) ) {
		require_once( get_template_directory() . '/include/admin-config.php' );
	}
}

// Load the default options
require_once( get_template_directory() . '/include/default-theme-options.php' );

if( !function_exists('grenada_get_theme_options') ){

	function grenada_get_theme_options( $option_id ){

		global $grenada_theme_options;
		$option_value = "";
		if( isset( $grenada_theme_options ) ){

			$option_value = $grenada_theme_options[ $option_id ];
		}

		return $option_value;

	}
}

if( !function_exists('grenada_get_post_meta') ){

	function grenada_get_post_meta( $opt_name = "", $thePost = array(), $meta_key = "", $def_val = "" ) {

		if( class_exists('Clapat\Grenada\Metaboxes\Meta_Boxes') ){
			
			return Clapat\Grenada\Metaboxes\Meta_Boxes::get_metabox_value( $thePost, $meta_key );
		}
		
		if( !function_exists('redux_post_meta') ){

			global $grenada_theme_options;
			return $grenada_theme_options[ $meta_key ];
		}
		else {

			return redux_post_meta( $opt_name, $thePost, $meta_key, $def_val );
		}

		return "";
	}
}

if( !function_exists('grenada_get_theme_options') ){

	function grenada_get_theme_options( $option_id ){

		global $grenada_theme_options;
		$option_value = "";
		if( isset( $grenada_theme_options ) ){

			$option_value = $grenada_theme_options[ $option_id ];
		}

		return $option_value;

	}
}

if( !function_exists('grenada_get_current_query') ){

	function grenada_get_current_query(){

		global $grenada_posts_query;
		global $wp_query;
		if( $grenada_posts_query == null ){
			return $wp_query;
		}
		else{
			return $grenada_posts_query;
		}

	}
}

// Hero properties
require_once ( get_template_directory() . '/include/hero-properties.php');
if( !function_exists('grenada_get_hero_properties') ){

	function grenada_get_hero_properties( $post_type ){

		global $grenada_hero_properties;
		if( !isset( $grenada_hero_properties ) || ( $grenada_hero_properties == null ) ){
			$grenada_hero_properties = new GrenadaHeroProperties();
		}
		$grenada_hero_properties->getProperties( $post_type );
		return $grenada_hero_properties;
	}
}

// Showcase slides
if( !function_exists('grenada_showcase_slides') ){

	function grenada_showcase_slides( $showcase_slides_param = null ){

		global $grenada_showcase_slides;
		if( isset( $showcase_slides_param ) && ( $showcase_slides_param != null ) ){
			
			$grenada_showcase_slides = $showcase_slides_param;
		}
		
		return $grenada_showcase_slides;
	}
}

// Portfolio Masonry images
if( !function_exists('grenada_portfolio_images') ){

	function grenada_portfolio_images( $portfolio_images_param = null ){

		global $grenada_portfolio_images;
		if( isset( $portfolio_images_param ) && ( $portfolio_images_param != null ) ){
			
			$grenada_portfolio_images = $portfolio_images_param;
		}
		
		return $grenada_portfolio_images;
	}
}

// Support for automatic plugin installation
require_once(  get_template_directory() . '/components/helper_classes/tgm-plugin-activation/class-tgm-plugin-activation.php');
require_once(  get_template_directory() . '/components/helper_classes/tgm-plugin-activation/required_plugins.php');

// Widgets
require_once(  get_template_directory() . '/components/widgets/widgets.php');

// Util functions
require_once ( get_template_directory() . '/include/util_functions.php');

// Add theme support
require_once ( get_template_directory() . '/include/theme-support-config.php');

// Theme setup
require_once ( get_template_directory() . '/include/setup-config.php');

// Enqueue scripts
require_once ( get_template_directory() . '/include/scripts-config.php');

// Hooks
require_once ( get_template_directory() . '/include/hooks-config.php');

// Blog comments and pagination
require_once ( get_template_directory() . '/include/blog-config.php');

// Visual Composer
if ( function_exists( 'vc_set_as_theme' ) ) {
	require_once ( get_template_directory() . '/include/vc-config.php');
}









?>
