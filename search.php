<?php
/**
 * The template for displaying Search Results pages
*/

get_header();

?>
		
	<!-- Main -->
	<div id="main">
	
		<!-- Hero Section -->
        <div id="hero">
           <div id="hero-styles">
                <div id="hero-caption" class="">
                    <div class="inner">
                        <h1 class="hero-title"><?php printf( esc_html__( 'Search Results for: %s', 'grenada'), get_search_query() ); ?></h1> 
                    </div>
                </div>                    
            </div>
        </div>                      
        <!--/Hero Section -->
		
	   	<!-- Main Content -->
    	<div id="main-content">
			<!-- Blog-->
			<div id="blog">
				<!-- Blog-Content-->
				<div id="blog-content">
					<?php

						if( have_posts() ){
						
							while( have_posts() ){

								the_post();

								get_template_part( 'sections/blog_post_section' );

							}
						} else{

							echo '<h4 class="search_results">' . esc_html__('No posts found', 'grenada') . '</h4>';

						}

					?>
					
				<!-- /Blog-Content -->
				</div>

				<?php

					grenada_pagination();
				?>
			</div>
			<!-- /Blog-->
		</div>
		<!--/Main Content-->
	</div>
	<!-- /Main -->
<?php

get_footer();

?>